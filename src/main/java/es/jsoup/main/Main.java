package es.jsoup.main;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Main {


	// URL a la que conectaremos
	public static final String url = "https://recetasgratis.net";
	
	
	public static void main(String[] args) throws Exception{
	
		List<String> listaLinks = null;
	
		//*******************************
		// Hacemos la consulta
		//*******************************
		Document doc = null;
		listaLinks = new ArrayList<String>();
		
		try {
			// Conectamos a la URL
			doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
			
			// Recogemos los href
		     Elements listaLinksAux = doc.select("a[href]");
		     
		     // Nos guardamos el texto y el hypervínculo
		     for (Element link :  listaLinksAux) {
		    		 listaLinks.add(link.text() + "  - " + link.attr("href"));
		     }
		     
		}catch (Exception e) {
			System.out.println("Error " + e.getMessage());
			e.printStackTrace();
		}
		
	    //********************************
		// Imprimimos los resultados
		//********************************
		for (String s : listaLinks) {
			System.out.println(s);
		}

	}

}
